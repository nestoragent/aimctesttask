package io.aimc.consumer;

import io.aimc.helpers.CSVHelper;

import java.util.HashSet;

/**
 * Created by nestor on 06.02.2019.
 */
public class Consumer implements Runnable {

    private String key;
    private HashSet<String> rows;

    public Consumer(String key, HashSet<String> rows) {
        this.key = key;
        this.rows = rows;
    }

    @Override
    public void run() {
        System.out.println("start consumer: " + Thread.currentThread().getName());
        CSVHelper.writeCSV(key, rows);
        System.out.println("end consumer: " + Thread.currentThread().getName());
        Thread.currentThread().interrupt();
    }
}
