package io.aimc.producer;

import io.aimc.helpers.CSVHelper;

import java.util.HashSet;
import java.util.Map;

/**
 * Created by nestor on 06.02.2019.
 */
public class Producer implements Runnable {

    private String path;
    private Map<String, HashSet<String>> resultArray;

    public Producer(String path, Map<String, HashSet<String>> resultArray) {
        this.path = path;
        this.resultArray = resultArray;
    }

    @Override
    public void run() {
        System.out.println("start producer: " + Thread.currentThread().getName());
        resultArray.putAll(CSVHelper.readCSV(path));
        System.out.println("end producer: " + Thread.currentThread().getName());
        Thread.currentThread().interrupt();
    }
}
