package io.aimc.helpers;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by nestor on 06.02.2019.
 */
public class CSVHelper {

    public static Map<String, HashSet<String>> readCSV(String path) {
        final Map<String, HashSet<String>> result = new HashMap<>();
        ArrayList<String> cols = new ArrayList<>();
        String line;
        String cvsSplitBy = ";";

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {

            Collections.addAll(cols, br.readLine().split(cvsSplitBy));
            cols.forEach(col -> result.put(col, new HashSet<>()));

            while ((line = br.readLine()) != null) {
                String[] items = line.split(cvsSplitBy);
                for (int i = 0; i < items.length; i++) {
                    HashSet<String> temp = result.get(cols.get(i));
                    temp.add(items[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void writeCSV(Map<String, HashSet<String>> result) {
        PrintWriter pw = null;

        for (String key : result.keySet()) {
            try {
                File file = new File("output/" + key + ".csv");
                if (!file.exists()) {
                    file.createNewFile();
                }
                pw = new PrintWriter(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            StringBuilder builder = new StringBuilder();
            HashSet<String> tempRow = result.get(key);
            tempRow.forEach(item -> builder.append(item).append(";"));
            pw.write(builder.toString());
            pw.close();
        }
    }

    public static void writeCSV(String key, HashSet<String> rows) {
        PrintWriter pw = null;

        try {
            File file = new File("output/" + key + ".csv");
            if (!file.exists())
                file.createNewFile();
            pw = new PrintWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder builder = new StringBuilder();
        rows.forEach(item -> builder.append(item).append(";"));
        pw.write(builder.toString());
        pw.close();
    }

    public static void createOutputFolder() {
        Path currentPath = Paths.get(System.getProperty("user.dir"));
        //set delimiter
        String del = currentPath.getFileSystem().toString().contains("WindowsFile") ? "\\" : "/";

        //create an output folder
        File folder = new File(currentPath.toString() + del + "output");
        if (!folder.exists())
            folder.mkdir();
    }

}
