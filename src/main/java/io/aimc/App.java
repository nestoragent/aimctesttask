package io.aimc;

import io.aimc.consumer.Consumer;
import io.aimc.helpers.CSVHelper;
import io.aimc.producer.Producer;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        CSVHelper.createOutputFolder();
        Map<String, HashSet<String>> resultArray = Collections.synchronizedMap(new HashMap<>());

        while (true) {
            ExecutorService consumerService = Executors.newFixedThreadPool(10);
            ExecutorService producerService = Executors.newFixedThreadPool(10);
            Scanner sc = new Scanner(System.in);
            System.out.println("Input paths: ");
            String inputString = sc.nextLine();

            for (String str : inputString.split(";")) {
                if (str.contains(".csv"))
                    producerService.submit(new Producer(str, resultArray));
            }
            producerService.shutdown();
            final boolean doneProducer = producerService.awaitTermination(10, TimeUnit.SECONDS);

            if (doneProducer || producerService.isShutdown()) {
                for (String key : resultArray.keySet()) {
                    consumerService.submit(new Consumer(key, resultArray.get(key)));
                }
            }
            consumerService.shutdown();
            final boolean doneConsumer = producerService.awaitTermination(10, TimeUnit.SECONDS);

            //clear map
            if (doneConsumer || consumerService.isShutdown()) {
                resultArray = Collections.synchronizedMap(new HashMap<>());
            }

            if ("".equals(inputString) || "done".equalsIgnoreCase(inputString)) {
                System.out.println("Inputted empty string or done. Exit");
                System.exit(0);
            }
        }

    }
}
